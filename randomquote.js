$(document).ready(function() {
  fetchQuotes();
  $("#btnNewQoute").click(function() {
    fetchQuotes();
  });

  function generateRandomIndex(total) {
    return Math.floor(Math.random() * (total + 1));
  }

  function generateRandomColor() {
    let letters = "0123456789ABCDEF";
    let color = "#";
    for (let i = 0; i < 6; i++) {
      let index = generateRandomIndex(15);
      color += letters[index];
    }
    return color;
  }

  function applyColor(color) {
    $("body").css("background-color", color);
    $("body").css("color", color);
    $(".button").css("background-color", color);
  }

  function updateQuote(quote) {
    $("#spnQuote").text(quote.quote);
    $("#spnAuthor").text("-" + quote.author);
  }

  function fetchQuotes() {
    fetch(
      "https://gist.githubusercontent.com/camperbot/5a022b72e96c4c9585c32bf6a75f62d9/raw/e3c6895ce42069f0ee7e991229064f167fe8ccdc/quotes.json"
    )
      .then(function(response) {
        if (response.status !== 200) {
          console.log(
            `Something went wrong. Response status: ${response.status}`
          );
          return;
        }

        response.json().then(function(data) {
          let quotes = data.quotes;
          let randomIndex = generateRandomIndex(quotes.length);
          let color = generateRandomColor(15);
          updateQuote(quotes[randomIndex]);
          applyColor(color);
        });
      })
      .catch(function(err) {
        console.log("Fetch Error: " + err);
      });
  }
});
